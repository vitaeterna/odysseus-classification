package prediction.transform;

import de.uniol.inf.is.odysseus.core.server.planmanagement.TransformationConfiguration;
import de.uniol.inf.is.odysseus.ruleengine.rule.RuleException;
import de.uniol.inf.is.odysseus.ruleengine.ruleflow.IRuleFlowGroup;
import de.uniol.inf.is.odysseus.transform.flow.TransformRuleFlowGroup;
import de.uniol.inf.is.odysseus.transform.rule.AbstractTransformationRule;
import prediction.logicaloperator.ActivityPredictionAO;
import prediction.physicaloperator.ActivityPredictionPO;

@SuppressWarnings({"rawtypes"})
public class TActivityPredictionAORule extends AbstractTransformationRule<ActivityPredictionAO> {

	@Override
	public int getPriority() {
		return 0;
	}

	@Override
	public void execute(ActivityPredictionAO predictionAO, TransformationConfiguration config) throws RuleException {
		defaultExecute(predictionAO, new ActivityPredictionPO(predictionAO), config, true, true);
	}

	@Override
	public boolean isExecutable(ActivityPredictionAO operator, TransformationConfiguration transformConfig) {
		return operator.isAllPhysicalInputSet();
	}

	@Override
	public String getName() {
		return "ActivityPredictionAO -> ActivityPredictionPO";
	}
	
	@Override
	public IRuleFlowGroup getRuleFlowGroup() {
		return TransformRuleFlowGroup.TRANSFORMATION;
	}
	
	@Override
	public Class<? super ActivityPredictionAO> getConditionClass() {	
		return ActivityPredictionAO.class;
	}

}
